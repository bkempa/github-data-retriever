# GitHub Data Retriever API
This project provides an API endpoint that allows API consumers to retrieve information about a GitHub user's repositories. It supports listing all repositories that are not forks and provides detailed information such as repository name, owner login, branch names, and the last commit SHA for each branch.

## Installation 
### Maven
To run the project using Maven, follow these steps:
1. Clone the repository to your local machine.
2. Navigate to the project's root directory.
3. Open a terminal or command prompt in that directory.
4. Run the following command to build the project:

```bash
mvn clean install
```
After a successful build, you can run the application using the following command:
```bash
mvn spring-boot:run
```
The API will be available at http://localhost:8080/{username}.

### Docker
To run the project using Docker, you can use the provided Dockerfile. Follow these steps:
1. Clone the repository to your local machine.
2. Navigate to the project's root directory.
3. Open a terminal or command prompt in that directory.
4. Build the Docker image using the following command:


```bash
docker build -t github-data-retriever-api .
```
After the image is built successfully, you can run a Docker container with the following command:
```
docker run -d -p 8080:8080 --name github-data-retriever github-data-retriever-api
```
The API will be available at http://localhost:8080/{username}.

## Usage
### List User Repositories
To retrieve a list of repositories for a given GitHub user, make a GET request to the following endpoint:

```bash
GET /{username}
```
The request should include the header `Accept: application/json` to specify the desired response format.

The API will respond with a JSON object containing the requested information:

    
```json
{
    "repository='Repository Name', owner='Owner Login'": [
        {
            "name": "Branch Name",
            "commit": {
                "sha": "Commit SHA"
            }
        },
        ...
    ],
    ...
}
```
### Error Responses
#### User Not Found (404)
If the specified GitHub user does not exist, the API will respond with a 404 status code and the following JSON object:

```json
{
    "status": 404,
    "message": "User not found"
}
```
#### Invalid Accept Header (406)
If the request includes `Accept: application/xml` header, the API will respond with a 406 status code and the following JSON object:
```json
{
    "status": 406,
    "message": "Unsupported media type"
}
```

#### Unsupported Media Type (415)
If the request includes an unsupported `Accept` header other than `application/json` or `application/xml`, the API will respond with a 415 status code and the following JSON object:
```json
{
  "status": 406,
  "message": "XML format not supported"
}
```
## Examples
### Example Request

```bash
GET /api/repositories?username=bkempa
Accept: application/json
```
### Example Response

```json
{
    "repository='.NET', owner='bkempa'": [
        {
            "name": "master",
            "commit": {
                "sha": "3eb6aa98333ec711c27fb87a0a0fb92fb5fc9a1e"
            }
        }
    ]
}
```