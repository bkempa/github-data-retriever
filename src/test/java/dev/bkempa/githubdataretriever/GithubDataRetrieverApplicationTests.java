package dev.bkempa.githubdataretriever;

import dev.bkempa.githubdataretriever.controller.Controller;
import dev.bkempa.githubdataretriever.exception.UserNotFoundException;
import dev.bkempa.githubdataretriever.model.Branch;
import dev.bkempa.githubdataretriever.model.Commit;
import dev.bkempa.githubdataretriever.model.GithubRepository;
import dev.bkempa.githubdataretriever.model.Owner;
import dev.bkempa.githubdataretriever.service.AppService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(Controller.class)
public class GithubDataRetrieverApplicationTests {

    @MockBean
    private AppService appService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetUserRepositories() throws Exception {
        var repository = new GithubRepository();
        repository.setName(".NET");
        var owner = new Owner();
        owner.setLogin("bkempa");
        repository.setOwner(owner);
        var commit = new Commit();
        commit.setSha("3eb6aa98333ec711c27fb87a0a0fb92fb5fc9a1e");
        var branch = new Branch();
        branch.setName("master");
        branch.setCommit(commit);

        var repositoryMap = Map.of(repository, List.of(branch));

        when(appService.getUsersRepositories(any(String.class), any(String.class))).thenReturn(repositoryMap);

        mockMvc.perform(get("/bkempa")
                        .header("Accept", "application/json"))
                .andExpect(status().isOk())
                .andExpect(content().json("""
                {
                    "repository='.NET', owner='bkempa'": [
                        {
                            "name": "master",
                            "commit": {
                                "sha": "3eb6aa98333ec711c27fb87a0a0fb92fb5fc9a1e"
                            }
                        }
                    ]
                }
                """));

        verify(appService, times(1)).getUsersRepositories("bkempa", "application/json");
        verifyNoMoreInteractions(appService);
    }

    @Test
    public void testGetUserRepositories_UserNotFound() throws Exception {
        when(appService.getUsersRepositories(any(String.class), any(String.class))).thenThrow(new UserNotFoundException());

        mockMvc.perform(get("/kauefhaiufhaiufhisaufh")
                        .header("Accept", "application/json"))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("User not found"));

        verify(appService, times(1)).getUsersRepositories("kauefhaiufhaiufhisaufh", "application/json");
        verifyNoMoreInteractions(appService);
    }

    @Test
    void testGetUserRepositories_XmlNotSupported() throws Exception {
        mockMvc.perform(get("/bkempa")
                        .header("Accept", "application/xml"))
                .andExpect(status().isNotAcceptable())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"message\": \"Media type not acceptable\"}"));
    }


}