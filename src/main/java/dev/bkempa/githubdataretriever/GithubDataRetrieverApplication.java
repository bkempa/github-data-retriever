package dev.bkempa.githubdataretriever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GithubDataRetrieverApplication {

    public static void main(String[] args) {
        SpringApplication.run(GithubDataRetrieverApplication.class, args);
    }

}
