package dev.bkempa.githubdataretriever.exception;

public class XmlFormatNotSupportedException extends RuntimeException {
    public XmlFormatNotSupportedException() {
        super();
    }
}
