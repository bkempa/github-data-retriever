package dev.bkempa.githubdataretriever.controller;

import dev.bkempa.githubdataretriever.model.Branch;
import dev.bkempa.githubdataretriever.model.GithubRepository;
import dev.bkempa.githubdataretriever.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class Controller {
    private final AppService appService;

    @Autowired
    public Controller(AppService appService) {
        this.appService = appService;
    }

    @GetMapping("/{username}")
    public Map<GithubRepository, List<Branch>> getRepositories(@PathVariable String username,
                                                               @RequestHeader("Accept") String acceptHeader) {
        return appService.getUsersRepositories(username, acceptHeader);
    }
}
