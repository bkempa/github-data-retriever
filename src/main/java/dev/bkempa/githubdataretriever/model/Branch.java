package dev.bkempa.githubdataretriever.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Branch {
    private String name;

    private Commit commit;

    private boolean ifProtected;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Commit getCommit() {
        return commit;
    }

    public void setCommit(Commit commit) {
        this.commit = commit;
    }

    @JsonIgnore
    public boolean isProtected() {
        return ifProtected;
    }

    @JsonProperty("protected")
    public void setIfProtected(boolean ifProtected) {
        this.ifProtected = ifProtected;
    }
}
