package dev.bkempa.githubdataretriever.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Commit {
    private String sha;
    @JsonIgnore
    private String url;
}
