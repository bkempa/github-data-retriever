package dev.bkempa.githubdataretriever.service;

import dev.bkempa.githubdataretriever.exception.UnsupportedMediaTypeException;
import dev.bkempa.githubdataretriever.exception.UserNotFoundException;
import dev.bkempa.githubdataretriever.exception.XmlFormatNotSupportedException;
import dev.bkempa.githubdataretriever.model.Branch;
import dev.bkempa.githubdataretriever.model.GithubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AppService {

    private final WebClient webClient;

    @Autowired
    public AppService(WebClient webClient) {
        this.webClient = webClient;
    }

    public Map<GithubRepository, List<Branch>> getUsersRepositories(String username, String acceptHeader) {
        return switch (acceptHeader.toLowerCase()) {
            case "application/json" -> {
                if (ifUserExist(username)) {
                    List<GithubRepository> repositories = getRepositories(username);
                    yield repositories.stream()
                            .filter(repository -> !repository.isFork())
                            .collect(Collectors.toMap(
                                    repository -> repository,
                                    this::getBranches
                            ));
                } else
                    throw new UserNotFoundException();
            }
            case "application/xml" -> throw new XmlFormatNotSupportedException();
            default -> throw new UnsupportedMediaTypeException();
        };
    }

    private List<GithubRepository> getRepositories(String username) {
        return webClient.get()
                .uri("https://api.github.com/users/{username}/repos", username)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<GithubRepository>>() {})
                .onErrorResume(e -> Mono.error(new UserNotFoundException()))
                .block();
    }

    private List<Branch> getBranches(GithubRepository repository) {
        return webClient.get()
                .uri("https://api.github.com/repos/" + repository.getFullName() + "/branches")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Branch>>() {})
                .block();
    }

    private boolean ifUserExist(String username) {
        try {
            webClient.get()
                    .uri("https://api.github.com/users/{username}", username)
                    .retrieve()
                    .toBodilessEntity()
                    .block();
            return true;
        } catch (WebClientResponseException ex) {
            if (ex.getStatusCode() == HttpStatus.NOT_FOUND)
                throw new UserNotFoundException();
            else
                throw ex;
        }
    }
}
