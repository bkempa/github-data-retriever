FROM bellsoft/liberica-openjdk-alpine-musl:17

WORKDIR /app
COPY target/github-data-retriever-0.0.1-SNAPSHOT.jar github-data-retriever.jar
CMD ["java", "-jar", "github-data-retriever.jar"]